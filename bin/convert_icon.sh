for i in 72 96 128 144 152 192 256 384 512 512; do
  convert -scale ${i} src/assets/icons/icon.svg src/assets/icons/icon-${i}x${i}.png
done

convert -scale 48 src/assets/icons/icon.svg src/favicon.ico
