const replace = require('replace-in-file')
const {LocalDateTime, DateTimeFormatter, DateTimeFormatterBuilder} = require("@js-joda/core");

const version = LocalDateTime.now().format(DateTimeFormatter.ofPattern('yyyy.MM.dd HH:mm:ss'))
console.log('going to replace BUILD_VERSION by ' + version)

const result = replace.sync({
  files: 'src/environments/environment.ts',
  from: /_timestamp_/g,
  to: version
})

console.log(result)

if (result == null || result.length < 1) {
  throw Error('No version file found.')
}
