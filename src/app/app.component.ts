import {ChangeDetectionStrategy, Component, ElementRef, HostListener, OnInit, signal, ViewChild} from '@angular/core'
import {all, BigNumber, ConfigOptions, create} from 'mathjs'
import {
  AbstractCalculatorAction,
  BackspaceAction,
  BracketAction,
  ChangeKeypadAction,
  EqualsAction,
  InputAction,
  InputFunctionAction,
  LoadStackAction,
  NegateAction,
  NoopAction,
  ResetAction,
  SettingsAction,
  StackAction
} from './common-actions'
import {SwUpdate} from '@angular/service-worker'
import {IndexedDbService} from './services/indexed-db.service'
import {MatIconModule} from '@angular/material/icon'
import {KeyComponent} from './key/key.component'
import {FormsModule} from '@angular/forms'
import {AsyncPipe, NgFor, NgIf} from '@angular/common'
import {MatListModule} from '@angular/material/list'

export interface StackEntry {
  stackId: string
  expression: string
  result: BigNumber | null | string
  hasError: boolean
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatListModule, NgFor, FormsModule, KeyComponent, NgIf, MatIconModule, AsyncPipe]
})
export class AppComponent implements OnInit {

  @ViewChild('currentExpressionInput')
  currentExpressionInput: ElementRef<HTMLInputElement>

  readonly currentExpression = signal<string>('')

  readonly currentResult = signal<BigNumber | null>(null)

  readonly currentResultHasError = signal<boolean>(false)

  readonly resultStack = signal<StackEntry[]>([])

  readonly editResultStack = signal<StackEntry | null>(null)

  readonly keypadPage = signal<number>(0)

  readonly triggerOnUp = signal<boolean>(false)

  readonly keypad: AbstractCalculatorAction[][][] = [
    [
      [new SettingsAction(this), new ChangeKeypadAction(this, 1), new ResetAction(this), new BackspaceAction(this)],
      [new BracketAction(this, '('), new BracketAction(this, ')'), new StackAction(this), new InputAction(this, '/')],
      [new InputAction(this, 7), new InputAction(this, 8), new InputAction(this, 9), new InputAction(this, '*')],
      [new InputAction(this, 4), new InputAction(this, 5), new InputAction(this, 6), new InputAction(this, '-')],
      [new InputAction(this, 1), new InputAction(this, 2), new InputAction(this, 3), new InputAction(this, '+')],
      [new InputAction(this, 0), new InputAction(this, '.'), new NegateAction(this), new EqualsAction(this)]
    ],
    [
      [new ChangeKeypadAction(this, 0), new ChangeKeypadAction(this, 2), new ResetAction(this), new BackspaceAction(this)],
      [new InputFunctionAction(this, 'floor'), new InputFunctionAction(this, 'ceil'), new InputFunctionAction(this, 'round', {funcEpilog: ',2'}), new NoopAction(this)],
      [new InputFunctionAction(this, '1/', {label: '1/x'}), new NoopAction(this), new NoopAction(this), new NoopAction(this)],
      [new NoopAction(this), new NoopAction(this), new NoopAction(this), new NoopAction(this)],
      [new NoopAction(this), new NoopAction(this), new NoopAction(this), new NoopAction(this)],
      [new NoopAction(this), new NoopAction(this), new NoopAction(this), new NoopAction(this)]
    ],
    [
      [new ChangeKeypadAction(this, 1), new ChangeKeypadAction(this, 0), new ResetAction(this), new BackspaceAction(this)],
      [new LoadStackAction(this, 'BeharPay', [
        '0',
        '4.4',
        's1/s2',
        'floor(s3)',
        's4*s2',
        's1-s5'
      ]),
        new NoopAction(this), new NoopAction(this), new NoopAction(this)],
      [new NoopAction(this), new NoopAction(this), new NoopAction(this), new NoopAction(this)],
      [new NoopAction(this), new NoopAction(this), new NoopAction(this), new NoopAction(this)],
      [new NoopAction(this), new NoopAction(this), new NoopAction(this), new NoopAction(this)],
      [new NoopAction(this), new NoopAction(this), new NoopAction(this), new NoopAction(this)]
    ]
  ]


  private readonly config: ConfigOptions = {
    epsilon: 1e-12,
    matrix: 'Matrix',
    number: 'BigNumber',
    precision: 24,
    predictable: false,
    randomSeed: null
  }
  private readonly math = create(all, this.config)

  constructor(private readonly swUpdate: SwUpdate,
              private readonly indexedDbService: IndexedDbService) {
  }

  getCurrentKeypad(): AbstractCalculatorAction[][] {
    return this.keypad[this.keypadPage()]
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(e: KeyboardEvent) {
    if (e.key?.length > 0) {
      const action = this.getCurrentKeypad().flat().find(key => key.matches(e.key))
      if (action != null) {
        this.enter(action)

        e.preventDefault()
        e.stopPropagation()
      }
    }
  }

  enter(numberOrString: AbstractCalculatorAction) {
    numberOrString.execute(this.currentExpressionInput.nativeElement.selectionStart ?? 0, this.currentExpressionInput.nativeElement.selectionEnd ?? 0)

    this.evaluate()
  }

  evaluate(): boolean {
    try {
      let seenError = false
      const scope = {} as any
      this.resultStack().forEach((value, index) => {
        if (!seenError) {
          try {
            value.result = this.math.evaluate(value?.expression, scope)
          } catch (e) {
            value.result = '#error?'
            value.hasError = true
            seenError = true
          }
        } else {
          value.result = '#previous_error?'
          value.hasError = true
        }

        scope['S' + (index + 1)] = value.result
        scope['s' + (index + 1)] = value.result
      })

      this.currentResult.set(this.math.evaluate(this.currentExpression(), scope))

      this.currentResultHasError.set(false)

      return true
    } catch (e) {
      this.currentResultHasError.set(true)
      console.log(e)

      return false
    }
  }

  pushResultOnStack() {
    if (
      this.isMeaningfulExpression(this.currentExpression())
      && this.evaluate()
      && this.currentResult() != null) {
      if (this.editResultStack() != null) {
        this.editResultStack()!.expression = this.currentExpression()
        this.editResultStack.set(null)

        this.currentExpression.set(this.resultStack()[this.resultStack.length - 1]?.stackId)

      } else if (
        this.resultStack.length == 0
        || this.resultStack()[this.resultStack.length - 1]?.expression !== this.currentExpression()) {
        const stackId = 's' + (this.resultStack.length + 1)
        this.resultStack.update(u => {
          u.push({
            stackId: stackId,
            expression: this.currentExpression(),
            result: this.currentResult(),
            hasError: false
          })
          return u
        })
        this.currentExpression.set(stackId)

        setTimeout(() => {
          const element = document.querySelector('.stack .last-entry')
          element?.scrollIntoView({behavior: 'smooth', block: 'end'})
        })
      }
    }
  }

  editStackEntry(stackEntry: StackEntry) {
    this.editResultStack.set(stackEntry)
    this.currentExpression.set(stackEntry.expression)

    this.evaluate()
  }

  ngOnInit(): void {
    console.log('swUpdate', this.swUpdate.isEnabled)
    this.reloadConfig()

    if (this.swUpdate.isEnabled) {
      this.swUpdate.activateUpdate().then(updated => {
        if (updated) {
          window.location.reload()
        }
      })
    }

    setTimeout(() => {
      this.currentExpressionInput.nativeElement.focus()
    }, 100)
  }

  public reloadConfig() {
    this.indexedDbService.getInt('font-size').then(fontSize => {
      document.documentElement.style.fontSize = fontSize + 'px'
    })
    this.indexedDbService.getBool('trigger-on-keyup').then(
      triggerOnKeyUp => this.triggerOnUp.set(triggerOnKeyUp)
    )
  }

  switchKepad(page: number) {
    this.keypadPage.set(page)
  }

  format(number: math.BigNumber | null): any {
    if (number == null) {
      return null
    }

    const roundedNumber = number.toFixed()
    const [integerPart, decimalPart] = roundedNumber.split('.')
    const formattedIntegerPart = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    const ret = decimalPart ? `${formattedIntegerPart}.${decimalPart}` : formattedIntegerPart
    return ret
  }

  private isMeaningfulExpression(expression: string): boolean {
    if (expression == null || expression.length < 1) {
      return false
    }

    if (expression.match('^S[0-9]+$')) {
      // only a stack reference
      return false
    }

    return expression.length > 0
  }
}
