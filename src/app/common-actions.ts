import {AppComponent, StackEntry} from './app.component'
import {MatDialog} from '@angular/material/dialog'
import {inject} from '@angular/core'
import {SettingsComponent} from './settings/settings.component'

export abstract class AbstractCalculatorAction {
  constructor(readonly _appComponent: AppComponent) {
  }

  abstract label(): string

  icon(): string | null {
    return null
  }

  matches(key: string): boolean {
    return this.label() === key
  }

  abstract execute(selectionStart: number, selectionEnd: number): void

  noBorder(): boolean {
    return false
  }

  readOnly(): boolean {
    return false
  }
}

export class EqualsAction extends AbstractCalculatorAction {
  execute(): void {
    this._appComponent.pushResultOnStack()
  }

  label(): string {
    return '='
  }
}

export class NoopAction extends AbstractCalculatorAction {
  execute(): void {
  }

  label(): string {
    return ''
  }

  override noBorder(): boolean {
    return true
  }


  override readOnly(): boolean {
    return true
  }
}

export class InputAction extends AbstractCalculatorAction {
  constructor(override readonly _appComponent: AppComponent,
              readonly input: string | number,
              readonly labelOverride?: string) {
    super(_appComponent)
  }

  execute(selectionStart: number, selectionEnd: number): void {
    this._appComponent.currentExpression.update(e => e.substring(0, selectionStart) + this.input + e.substring(selectionEnd))
    setTimeout(() => {
      this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(selectionStart + 1, selectionStart + 1)
    })
  }

  label(): string {
    return this.labelOverride ?? this.input.toString()
  }
}

export class BracketAction extends AbstractCalculatorAction {
  constructor(override readonly _appComponent: AppComponent,
              readonly input: string | number) {
    super(_appComponent)
  }

  execute(selectionStart: number, selectionEnd: number): void {
    if (selectionStart != selectionEnd) {
      this._appComponent.currentExpression.update(e => e.substring(0, selectionStart)
        + '('
        + e.substring(selectionStart, selectionEnd)
        + ')'
        + e.substring(selectionEnd))
    } else {
      this._appComponent.currentExpression.update(e => e.substring(0, selectionStart) + this.input + e.substring(selectionEnd))
    }
    setTimeout(() => {
      if (selectionStart != selectionEnd && this.input == ')') {
        this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(selectionEnd + 2, selectionEnd + 2)
      } else {
        this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(selectionStart + 1, selectionStart + 1)
      }
    })
  }

  label(): string {
    return this.input.toString()
  }
}

export class BackspaceAction extends AbstractCalculatorAction {
  execute(selectionStart: number, selectionEnd: number): void {
    if (selectionStart === selectionEnd) {
      selectionStart = selectionStart - 1
    }

    this._appComponent.currentExpression.update(e => e.substring(0, selectionStart) + e.substring(selectionEnd))
    setTimeout(() => {
      this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(selectionStart, selectionStart)
    })
  }


  override matches(key: string): boolean {
    return key === 'Backspace'
  }

  override icon(): string | null {
    return 'backspace'
  }

  label(): string {
    return ''
  }
}

export class StackAction extends AbstractCalculatorAction {
  execute(selectionStart: number, selectionEnd: number): void {
    if (selectionStart != selectionEnd) {
      this._appComponent.currentExpression.update(e => e.substring(0, selectionStart)
        + 's'
        + e.substring(selectionEnd))
    } else {
      let token = this._appComponent.currentExpression().substring(selectionStart - 1, selectionStart)
      if (token === 's') {
        this._appComponent.currentExpression.update(e => e.substring(0, selectionStart - 1)
          + 'S'
          + e.substring(selectionEnd))

        setTimeout(() => {
          this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(selectionStart, selectionStart)
        })
      } else if (token === 'S') {
        this._appComponent.currentExpression.update(e => e.substring(0, selectionStart - 1)
          + 's'
          + e.substring(selectionEnd))
        setTimeout(() => {
          this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(selectionStart, selectionStart)
        })
      } else {
        this._appComponent.currentExpression.update(e => e.substring(0, selectionStart)
          + 's'
          + e.substring(selectionEnd))
        setTimeout(() => {
          this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(selectionStart + 1, selectionStart + 1)
        })
      }
    }
  }


  override matches(key: string): boolean {
    return key === 'S' || key === 's'
  }

  label(): string {
    return 'Stack'
  }
}

export class ResetAction extends AbstractCalculatorAction {
  execute(selectionStart: number, selectionEnd: number): void {
    this._appComponent.resultStack.update(s => {
      s.splice(0)
      return s
    })
    this._appComponent.currentExpression.set('')
  }


  override matches(key: string): boolean {
    return key === 'R' || key === 'r'
  }

  label(): string {
    return 'Reset'
  }
}

export class SettingsAction extends AbstractCalculatorAction {

  readonly _dialog = inject(MatDialog)

  execute(selectionStart: number, selectionEnd: number): void {
    this._dialog.open(SettingsComponent, {
      width: '90%',
      height: '90%'
    }).afterClosed()
      .subscribe(_ => this._appComponent.reloadConfig())
  }

  label(): string {
    return ''
  }

  override icon(): string | null {
    return 'settings'
  }
}

export class NegateAction extends AbstractCalculatorAction {
  execute(selectionStart: number, selectionEnd: number): void {
    if (this._appComponent.currentExpression().length == 0) {
      return
    }

    if (this._appComponent.currentExpression().startsWith('-(') && this._appComponent.currentExpression().endsWith(')')) {
      this._appComponent.currentExpression.update(e => e.substring(2, this._appComponent.currentExpression().length - 1))
    } else {
      this._appComponent.currentExpression.update(e => '-(' + e + ')')
    }
  }

  override matches(key: string): boolean {
    return key === 'N' || key === 'n'
  }

  label(): string {
    return '+/-'
  }
}

export class ChangeKeypadAction extends AbstractCalculatorAction {
  constructor(override readonly _appComponent: AppComponent,
              readonly page: number) {
    super(_appComponent)
  }

  execute(selectionStart: number, selectionEnd: number): void {
    this._appComponent.switchKepad(this.page)
  }

  override matches(key: string): boolean {
    return false
  }


  override icon(): string | null {
    return 'grid_on'
  }

  label(): string {
    return this.page.toString()
  }
}

export class InputFunctionAction extends AbstractCalculatorAction {
  constructor(override readonly _appComponent: AppComponent,
              readonly func: string,
              readonly options?: {
                readonly label?: string,
                readonly funcProlog?: string,
                readonly funcEpilog?: string
              }) {
    super(_appComponent)
  }

  execute(selectionStart: number, selectionEnd: number): void {
    if (selectionStart === selectionEnd) {
      this._appComponent.currentExpression.update(e => e.substring(0, selectionStart)
        + this.func
        + '('
        + (this.options?.funcProlog ?? '')
        + (this.options?.funcEpilog ?? '')
        + ')'
        + e.substring(selectionEnd))

      const cursorPos = selectionStart + this.func.length + 1 + (this.options?.funcProlog?.length ?? 0)
      setTimeout(() => {
        this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(cursorPos, cursorPos)
      })

    } else {
      this._appComponent.currentExpression.update(e => e.substring(0, selectionStart)
        + this.func
        + '('
        + (this.options?.funcProlog ?? '')
        + e.substring(selectionStart, selectionEnd)
        + (this.options?.funcEpilog ?? '')
        + ')'
        + e.substring(selectionEnd))

      const cursorPos = selectionEnd + this.func.length + 1 + (this.options?.funcProlog?.length ?? 0)
      setTimeout(() => {
        this._appComponent.currentExpressionInput.nativeElement.setSelectionRange(cursorPos, cursorPos)
      })
    }
  }

  label(): string {
    return this.options?.label ?? this.func
  }
}

export class LoadStackAction extends AbstractCalculatorAction {
  constructor(override readonly _appComponent: AppComponent,
              readonly name: string,
              readonly stackEntries: string[]) {
    super(_appComponent)
  }

  execute(selectionStart: number, selectionEnd: number): void {
    const loadedStackEntries: StackEntry[] = this.stackEntries.map((stackEntry, index) => {
      const stackId = 's' + (index + 1)
      return {
        stackId: stackId,
        expression: stackEntry,
        result: '',
        hasError: false
      } as StackEntry
    })

    this._appComponent.resultStack.update(s => {
      s.splice(0)
      s.push(...loadedStackEntries)
      return s
    })

    this._appComponent.currentExpression.set('')
    this._appComponent.switchKepad(0)
  }

  override matches(key: string): boolean {
    return false
  }

  label(): string {
    return this.name
  }
}
