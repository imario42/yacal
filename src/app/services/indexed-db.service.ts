import {Injectable} from '@angular/core'
import {createStore, del, get, set, UseStore} from 'idb-keyval'

@Injectable({
  providedIn: 'root'
})
export class IndexedDbService {

  readonly customStore: UseStore

  constructor() {
    this.customStore = createStore('yacal-datenwort-at-db', 'keyval')
  }

  deleteKey(key: string): Promise<void> {
    const ret = del(key, this.customStore)
    return ret
  }

  getString(key: string): Promise<string | null> {
    const ret = get(key, this.customStore).then(value => value?.toString())
    return ret
  }

  setString(key: string, value: string): Promise<void> {
    const ret = set(key, value, this.customStore)
    return ret
  }

  getInt(key: string): Promise<number | null> {
    const ret = get(key, this.customStore).then(value => value != null ? parseInt(value) : null)
    return ret
  }

  setInt(key: string, value: number): Promise<void> {
    const ret = set(key, value?.toString(), this.customStore)
    return ret
  }

  getBool(key: string): Promise<boolean> {
    const ret = get(key, this.customStore).then(value => value === 'true')
    return ret
  }

  setBool(key: string, value: boolean): Promise<void> {
    const ret = set(key, value ? 'true' : 'false', this.customStore)
    return ret
  }
}
