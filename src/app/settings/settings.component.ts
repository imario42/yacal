import {ChangeDetectionStrategy, Component, isDevMode, OnInit, signal} from '@angular/core'
import {SwUpdate} from '@angular/service-worker'
import {
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle
} from '@angular/material/dialog'
import {environment} from '../../environments/environment'
import {IndexedDbService} from '../services/indexed-db.service'
import {MatButtonModule} from '@angular/material/button'
import {NgIf} from '@angular/common'
import {MatCheckboxModule} from '@angular/material/checkbox'
import {MatButtonToggleModule} from '@angular/material/button-toggle'
import {FormsModule} from '@angular/forms'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatDialogTitle, MatDialogContent, FormsModule, MatButtonToggleModule, MatCheckboxModule, MatDialogActions, NgIf, MatButtonModule, MatDialogClose]
})
export class SettingsComponent implements OnInit {
  readonly version = signal<string>('?')

  readonly swUpdateEnabled = signal<boolean>(false)

  readonly isDevModeEnabled = signal<boolean>(false)

  readonly fontSize = signal<number>(16)

  readonly triggerOnKeyup = signal<boolean>(false)

  readonly updateAvailable = signal<boolean | null | string>(null)

  constructor(private readonly swUpdate: SwUpdate,
              private readonly dialogRef: MatDialogRef<any>,
              private readonly indexedDbService: IndexedDbService) {
    this.version.set(environment.version)
    this.swUpdateEnabled.set(swUpdate.isEnabled)
    this.isDevModeEnabled.set(isDevMode())
  }

  ngOnInit(): void {
    Promise.all([
      this.indexedDbService.getInt('font-size'),
      this.indexedDbService.getBool('trigger-on-keyup')
    ])
      .then(([fontSize, triggerOnKeyup]) => {
        if (fontSize != null) {
          this.fontSize.set(fontSize)
        }
        this.triggerOnKeyup.set(triggerOnKeyup)
      })
  }

  checkForUpdate() {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.checkForUpdate().then(result => {
          this.updateAvailable.set(result)
          if (result) {
            window.location.reload()
          }
        }
      )
    }
  }

  updateFontSize(fontSize: number) {
    this.indexedDbService.setInt('font-size', fontSize)
  }

  updateTriggerOnKeyup(triggerOnKeyup: boolean) {
    this.indexedDbService.setBool('trigger-on-keyup', triggerOnKeyup)
  }
}
