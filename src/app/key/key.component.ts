import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  Output
} from '@angular/core'
import {animate, AnimationBuilder, AnimationPlayer, style} from '@angular/animations'

@Component({
    selector: 'app-key',
    templateUrl: './key.component.html',
    styleUrls: ['./key.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class KeyComponent {

  @HostBinding('class.read-only')
  @Input()
  readOnly: boolean = false

  @Input()
  triggerOnUp: boolean = false

  @Output()
  readonly call = new EventEmitter()

  private animationPlayer: AnimationPlayer

  constructor(
    private hostElement: ElementRef,
    private readonly animationBuilder: AnimationBuilder) {

    const animationFactory = this.animationBuilder.build([
      style({
        backgroundColor: 'var(--ripple-color)'
      }),
      animate('100ms',
        style({
          backgroundColor: 'var(--key-background-color)'
        }))
    ])

    this.animationPlayer = animationFactory.create(this.hostElement.nativeElement)
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(e: KeyboardEvent) {
    if (this.triggerOnUp) {
      return
    }
    this.trigger(e)
  }

  @HostListener('mousedown', ['$event'])
  onMouseDown(e: MouseEvent) {
    if (this.triggerOnUp) {
      return
    }

    this.trigger(e)
  }

  @HostListener('touchstart', ['$event'])
  onTouchStart(e: TouchEvent) {
    if (this.triggerOnUp) {
      return
    }

    this.trigger(e)
  }

  @HostListener('keyup', ['$event'])
  onKeyUp(e: KeyboardEvent) {
    if (!this.triggerOnUp) {
      return
    }
    this.trigger(e)
  }

  @HostListener('click', ['$event'])
  onClick(e: TouchEvent) {
    if (!this.triggerOnUp) {
      return
    }

    this.trigger(e)
  }

  private trigger(e: Event) {
    if (this.readOnly) {
      return
    }

    this.animationPlayer.reset()
    this.animationPlayer.play()

    this.call.emit()

    e.preventDefault()
    e.stopPropagation()
  }
}
