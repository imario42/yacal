import {AppComponent} from './app/app.component'
import {MatButtonToggleModule} from '@angular/material/button-toggle'
import {MatCheckboxModule} from '@angular/material/checkbox'
import {MatSliderModule} from '@angular/material/slider'
import {MatListModule} from '@angular/material/list'
import {MatButtonModule} from '@angular/material/button'
import {MatDialogModule} from '@angular/material/dialog'
import {MatIconModule} from '@angular/material/icon'
import {FormsModule} from '@angular/forms'
import {importProvidersFrom, isDevMode} from '@angular/core'
import {ServiceWorkerModule} from '@angular/service-worker'
import {provideAnimations} from '@angular/platform-browser/animations'
import {bootstrapApplication, BrowserModule} from '@angular/platform-browser'


bootstrapApplication(AppComponent, {
    providers: [
        importProvidersFrom(BrowserModule, ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: !isDevMode(),
            // Register the ServiceWorker as soon as the application is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000'
        }), FormsModule, MatIconModule, MatDialogModule, MatButtonModule, MatListModule, MatSliderModule, MatCheckboxModule, MatButtonToggleModule),
        provideAnimations()
    ]
})
  .catch(err => console.error(err));
